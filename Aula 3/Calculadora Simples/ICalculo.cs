﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Tentando usar interface

namespace Calculadora
{
    public interface ICalculo
    {
        void Soma(double num1, double num2);
        void Subtração(double num1, double num2);

        void Divisão(double num1, double num2);

        void Multiplicação(double num1, double num2);

        double Resultado { get; }
    }
}