namespace Dev;

class Turma
{
    private List<Aluno> alunos = new List<Aluno>();

    public void AdicionarAluno(Aluno aluno)
    {
        alunos.Add(aluno);
    }


    public double CalcularMediaTurma()
    {
        if (alunos.Count == 0)
        {
            Console.WriteLine("Não há alunos na turma.");
            return 0.0;
        }

        double somaNotas = 0.0;
        foreach (var aluno in alunos)
        {
            somaNotas += aluno.Nota;
        }

        return somaNotas / alunos.Count;
    }

    public void ExibirAlunos()
    {
        Console.WriteLine("Alunos na turma:");
        foreach (var aluno in alunos)
        {
            Console.WriteLine($"Nome: {aluno.Nome}, Nota: {aluno.Nota}");
        }
    }
    
}
