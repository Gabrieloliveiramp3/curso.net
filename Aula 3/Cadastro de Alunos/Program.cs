﻿using System;
using System.Collections.Generic;
using Dev;



static class Program
{
    
    static void Main()
    {
        var turma = new Turma();

        // Adicionando alunos à turma
        turma.AdicionarAluno(new Aluno("Gabriel", 8.5));
        turma.AdicionarAluno(new Aluno("Bankai", 7.0));
        turma.AdicionarAluno(new Aluno("Doroteia", 9.2));
        turma.AdicionarAluno(new Aluno("Nicole", 10.0));    

        // Exibindo a lista de alunos
        turma.ExibirAlunos();

        // Calculando a média da turma
        double mediaTurma = turma.CalcularMediaTurma();
        Console.WriteLine($"Média da turma: {mediaTurma:F2}");
        Console.ReadKey();
    }
}
