﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hierarquia_de_Formas_Geométricas
{
    internal abstract class FormaGeometrica
    {
        public abstract double CalcularArea();

        public abstract double CalcularPerimetro();
    }

    // Exemplo de uma classe concreta que herda de FormaGeometrica
    internal class Retangulo : FormaGeometrica
    {
        private double baseRetangulo;
        private double altura;

        public Retangulo(double baseRetangulo, double altura)
        {
            this.baseRetangulo = baseRetangulo;
            this.altura = altura;
        }

        public override double CalcularArea()
        {
            return baseRetangulo * altura;
        }

        public override double CalcularPerimetro()
        {
            return 2 * (baseRetangulo + altura);
        }
    }

    internal class Circulo : FormaGeometrica
    {
        private double raio;

        public Circulo(double raio)
        {
            this.raio = raio;
        }

        public override double CalcularArea()
        {
            return Math.PI * raio * raio;
        }

        public override double CalcularPerimetro()
        {
            return 2 * Math.PI * raio;
        }
    }

    internal class Triangulo : FormaGeometrica
    {
        private double lado1;
        private double lado2;
        private double lado3;

        public Triangulo(double lado1, double lado2, double lado3)
        {
            this.lado1 = lado1;
            this.lado2 = lado2;
            this.lado3 = lado3;
        }

        public override double CalcularArea()
        {
            double s = (lado1 + lado2 + lado3) / 2;
            return Math.Sqrt(s * (s - lado1) * (s - lado2) * (s - lado3));
        }

        public override double CalcularPerimetro()
        {
            return lado1 + lado2 + lado3;
        }
    }
}