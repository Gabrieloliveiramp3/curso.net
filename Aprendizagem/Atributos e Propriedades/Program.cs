using System;


class HelloWorld
{
    static void Main()
    {


        /* o 1 Produto C) o tipo que a vC!riavel P serC!.
           Ou seja, a vC!riavel P C) do tipo Produto.
           o new Produto estC! instanciando o P com a classe Produto,
           com isso o P tem acesso aos atributos da classe e pode esta atribuindo valor a eles.
         */
        Produto p = new Produto();
        p.Nome = "pa";
        p.num1 = 10222;
  

        Console.WriteLine(p.Nome);
        Console.WriteLine(p.num1);
    }
}


A palavra-chave namespace é usada para declarar um escopo que contém um conjunto de objetos relacionados. Você pode usar um namespace para organizar elementos de código e criar tipos globalmente exclusivos.

No caso, voce coloca um namespace em uma classe, quando quiser chamar essa classe, tem que especificar em qual namespace essa classe esta, isso serve pra termos varias classes com nomes iguais e n ter conflito devido o nome igual

o namespace serve muito para organizar o código
digamos vc esteja criando um código para calcular diversas áreas... quadrados, triângulos e afins
ao invés de criar  uma classe para cada opção (Area_quadrado | Area_triangulo), vc só precisaria mudar o namespace... aí ficaria Quadrado.Area.resultado(2f, 4f) | Triangulo.Area.resultado(2f, 4f)



