public class Produto
{
    // nome eh um atributo da classe
    private string nome;

    

    //"Nome" eh uma propriedade.
    //uma property no C#, que é uma boa prática de programação porque permite encapsular o acesso aos atributos de uma classe, fornecendo controle, validação e a possibilidade de adicionar comportamentos adicionais ao acesso desses atributos;
    public string Nome
    {
        get
        {
            //quando essa propriedade for lida, o valor lido sera do atributo nome
            return nome.ToUpper();
        }
        set
        {
            /* quando alguem escrever algo nessa propriedade, faremos um teste de condicional,
               onde pegaremos o valor de caracteres e ela tera que ser mais do que 1 letras,
               caso nao, ela entra no else , ocorrendo um erro
             */
            if (value.Length > 1)
                nome = value;
            else
                throw new
                  Exception("Nome do Produto deve ter pelo menos 2 caracteres");
        }
    }

     //num1 é uma propriedade da classe
 public int num1
 {
     get
     {
         return Num1;
     }
     set
     {
         Num1 = value;
     }
 }


}
