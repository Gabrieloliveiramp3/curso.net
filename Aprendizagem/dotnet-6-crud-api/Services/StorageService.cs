﻿using System.Runtime.CompilerServices;

namespace WebApi.Services;

public class StorageService : IStorageService
{
    private Dictionary<int, string> _dadosDicionario;
    private int _proxima;

    public StorageService()
    {
        _dadosDicionario = new Dictionary<int, string>();
        _proxima = 1;
    }

    public void SaveData(string dados)
    {
        _dadosDicionario.Add(_proxima, dados);
        Console.WriteLine("Dados salvos: " + dados);
        _proxima++;
    }
}
