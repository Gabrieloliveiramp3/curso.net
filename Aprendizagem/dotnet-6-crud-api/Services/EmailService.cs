﻿namespace WebApi.Services;

public class EmailService : IEmailService
{
    private readonly IStorageService _storageService;

    public EmailService(IStorageService storageService)
    {
        _storageService = storageService;
    }

    public void SendEmail(string destinatario, string mensagem)
    {
        Console.WriteLine("Enviando e-mail para: " + destinatario);
        Console.WriteLine("Mensagem: " + mensagem);

        _storageService.SaveData(mensagem);
    }
}