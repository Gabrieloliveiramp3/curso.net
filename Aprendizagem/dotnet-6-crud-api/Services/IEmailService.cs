namespace WebApi.Services;

public interface IEmailService
{
    void SendEmail(string destinatario, string mensagem);
}