/*Nesse código, a classe EmailService depende apenas da interface IStorageService, que é uma abstração do serviço de armazenamento 
de dados. A classe EmailService não sabe nem se importa com qual implementação concreta de IStorageService está sendo usada, 
podendo ser DatabaseStorageService ou FileStorageService. A instância da implementação concreta é passada para a classe EmailService 
por meio de um construtor, que é uma forma de injeção de dependência. Assim, o código fica mais desacoplado, flexível e testável.



// Implementa a interface usando uma classe concreta
public class StorageService : IStorageService
{
    // Implementa os m�todos da interface
    public void SaveEmail(Email email)
    {
        // L�gica para salvar o e-mail em um banco de dados, por exemplo
    }

    public Email GetEmail(int id)
    {
        // L�gica para recuperar o e-mail de um banco de dados, por exemplo
        return new Email();
    }

    // Outros m�todos relevantes
}

// Define a classe que representa o servi�o de envio de e-mails
public class EmailService
{
    // Declara uma vari�vel do tipo da interface, em vez de uma classe concreta
    private IStorageService storageService;

    // Recebe uma inst�ncia da interface no construtor, em vez de criar uma nova inst�ncia de uma classe concreta
    public EmailService(IStorageService storageService)
    {
        this.storageService = storageService;
    }

    // Usa os m�todos da interface, em vez de chamar diretamente os m�todos de uma classe concreta
    public void SendEmail(Email email)
    {
        // L�gica para enviar o e-mail, por exemplo
        // ...

        // Salva o e-mail usando o servi�o de armazenamento de dados
        storageService.SaveEmail(email);
    }

    public Email ReadEmail(int id)
    {
        // Recupera o e-mail usando o servi�o de armazenamento de dados
        return storageService.GetEmail(id);
    }

    // Outros m�todos relevantes
}