<p><img align="right" src="Asset/programming_1.gif" alt="Gabrieloliveiramp3" width="450px"  /></p>

<h2>Curso .NET Backend (Onboarding)

Local de repositório das atividades e projetos durante o curso. 

Professor : Emerson Delatorre </h2>


### <h2> 🛠 &nbsp;Linguagem:</h2> 
![C#](https://img.shields.io/badge/c%23-%23239120.svg?style=for-the-badge&logo=csharp&logoColor=white)

### <h2> 💻  Tecnologia e ferramentas:</h2>
![.Net](https://img.shields.io/badge/.NET-5C2D91?style=for-the-badge&logo=.net&logoColor=white)
![Git](https://img.shields.io/badge/git-%23F05033.svg?style=for-the-badge&logo=git&logoColor=white)
![Visual Studio Code](https://img.shields.io/badge/Visual%20Studio%20Code-0078d7.svg?style=for-the-badge&logo=visual-studio-code&logoColor=white)
![Visual Studio](https://img.shields.io/badge/Visual%20Studio-5C2D91.svg?style=for-the-badge&logo=visual-studio&logoColor=white)

