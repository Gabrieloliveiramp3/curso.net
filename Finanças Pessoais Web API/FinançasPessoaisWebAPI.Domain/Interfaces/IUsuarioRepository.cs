using FinançasPessoaisWebAPI.Domain.Entities;

namespace FinançasPessoaisWebAPI.Domain.Interfaces;

public interface IUsuarioRepository
{
    // A interface IUsuarioRepository define os métodos que devem ser implementados pelas classes concretas que representam o repositório de usuários.

    Task<IEnumerable<Usuario>> GetAllUsuarioAsync();

    Task<Usuario> GetUsuarioByIdAsync(int id);

    Task<Usuario> CreateUsuarioAsync(Usuario usuario);

    Task<Usuario> EditUsuarioAsync(Usuario usuario);

    Task<Usuario> RemoveUsuario(int id);
}