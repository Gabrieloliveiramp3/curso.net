﻿using FinançasPessoaisWebAPI.Domain.Entities;

namespace FinançasPessoaisWebAPI.Domain.Interfaces
{
    public interface IContaRepository
    {
        Task<IEnumerable<Conta>> GetAllContaAsync();

        Task<Conta> GetContaByIdAsync(int id);

        Task<Conta> CreateContaAsync(Conta conta);

        Task<Conta> EditContaAsync(Conta conta);

        Task<Conta> RemoveConta(int id);
    }
}
