﻿using FinançasPessoaisWebAPI.Domain.Entities;

namespace FinançasPessoaisWebAPI.Domain.Interfaces;

public interface ITransacaoRepository
{
    Task<IEnumerable<Transacao>> GetAllTransacaoAsync();

    Task<Transacao> GetTransacaoByIdAsync(int id);

    Task<Transacao> CreateTransacaoAsync(Transacao transacao);

    Task<Transacao> EditTransacaoAsync(Transacao transacao);

    Task<Transacao> RemoveTransacao(int id);
}