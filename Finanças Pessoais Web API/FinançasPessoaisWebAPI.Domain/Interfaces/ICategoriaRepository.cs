﻿using FinançasPessoaisWebAPI.Domain.Entities;

namespace FinançasPessoaisWebAPI.Domain.Interfaces;

public interface ICategoriaRepository
{
    Task<IEnumerable<Categoria>> GetAllCategoriaAsync();

    Task<Categoria> GetCategoriaByIdAsync(int id);

    Task<Categoria> CreateCategoriaAsync(Categoria categoria);

    Task<Categoria> EditCategoriaAsync(Categoria categoria);

    Task<Categoria> RemoveCategoria(int id);
}