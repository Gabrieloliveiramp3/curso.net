﻿using System.ComponentModel.DataAnnotations;

namespace FinançasPessoaisWebAPI.Domain.Entities
{
    public class Conta
    {
        public int Id { get; set; }

        [MaxLength(10, ErrorMessage = "O campo Agencia pode até 10 Numeros")]
        public int Agencia { get; set; }

        [MaxLength(10, ErrorMessage = "O campo Numero pode até 10 Numeros")]
        public int Numero { get; set; }

        [MaxLength(20, ErrorMessage = "O campo Banco pode até 20 Caracteres")]
        public string? Banco { get; set; }

        public int UsuarioId { get; set; }
        //Uma conta pertence a um usuario

        public Usuario Usuario { get; set; }

        public ICollection<Transacao> Transacaos { get; } = new List<Transacao>();
        //Uma conta pode ter várias transações
    }
}