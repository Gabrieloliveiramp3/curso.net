﻿using System.ComponentModel.DataAnnotations;

namespace FinançasPessoaisWebAPI.Domain.Entities;

public class Transacao
{
    public int Id { get; set; }

    [Required(ErrorMessage = "O valor é obrigatório.")]
    public decimal Valor { get; set; }

    [MaxLength(50, ErrorMessage = "O campo Descrição pode até 50 caracteres")]
    public string? Descricao { get; set; }
    public DateTime Data { get; set; } = DateTime.Now;
    public int CategoriaId { get; set; }
    public virtual Categoria Categoria { get; set; }
    public virtual Usuario Usuario { get; set; }
    public int UsuarioId { get; set; }

    public int ContaId { get; set; }
    public virtual Conta Conta { get; set; }
}