﻿namespace FinançasPessoaisWebAPI.Domain.Entities;

public class Relatorio
{
    public bool tipo;
    public Categoria? categoria;

    public string? User { get; set; }
    public string? Tipo { get; internal set; }
    public string? Descricao { get; set; }
    public DateTime Data { get; set; }
    public decimal valor { get; internal set; }
}