﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace FinançasPessoaisWebAPI.Domain.Entities;

public class Usuario(string email, bool admin, byte[] senhaNormal, byte[] senhaHash)
{
    public int Id { get; set; }

    [MaxLength(50, ErrorMessage = "Nome do Usuário não deve ter mais que 50 caracteres")]
    public string NomeUsuario { get; set; } = string.Empty;

    [MaxLength(50, ErrorMessage = "O Email não deve ter mais que 50 caracteres")]
    [EmailAddress]
    public string Email { get; set; } = email;

    public bool Admin { get; set; } = false;
    public byte[] SenhaNormal { get; set; } = senhaNormal;
    public byte[] SenhaHash { get; set; } = senhaHash;

    public virtual Conta Conta { get; set; }
    //Um usuario tem uma conta

    [JsonIgnore]
    public ICollection<Transacao> Transacaos { get; } = new List<Transacao>();

    //Um usuario pode fazer várias transações

    public void AlterarSenha(byte[] senhaNormal, byte[] senhaHash)
    {
        SenhaNormal = senhaNormal;
        SenhaHash = senhaHash;
    }

    public void SetAdmin(bool admin)
    {
     Admin = admin;
    }
}

//O get permite obter o valor atual da propriedade, e o set permite definir um novo valor para ela.