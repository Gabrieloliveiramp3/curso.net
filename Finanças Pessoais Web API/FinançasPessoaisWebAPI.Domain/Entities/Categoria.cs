﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace FinançasPessoaisWebAPI.Domain.Entities;

public class Categoria
{
    public int Id { get; set; }

    [MaxLength(50)]
    public string? categoria { get; set; }
    //Essa propriedade serve para a pessoa descrever

    [Required, Range(typeof(bool), "true", "false", ErrorMessage = "O Tipo tem que ser True para Receitas e False para Despesas")]
    public bool Tipo { get; set; }

    // Uma categoria pode ter várias transações
    public ICollection<Transacao> Transacaos { get; set; }


    [Description("Receita")]
    public enum CategoriaReceita
    {
        Salário,
        Benefícios,
        Vendas,
        Economias,
        Outros
    }

    [Description("Despesas")]
    public enum CategoriaDespesa
    {
        Saúde,
        Aluguel,
        Educação,
        Lazer,
        Alimentação,
        Despesas_pessoais,
        Outros
    }

    // Validação customizada para a propriedade categoria
    public class CategoriaValidationAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var categoria = value?.ToString();
            var tipo = (bool)validationContext.ObjectInstance.GetType().GetProperty("Tipo").GetValue(validationContext.ObjectInstance);

            if (tipo && !Enum.IsDefined(typeof(CategoriaDespesa), categoria))
            {
                return new ValidationResult("Categoria inválida para despesas.");
            }

            if (!tipo && !Enum.IsDefined(typeof(CategoriaReceita), categoria))
            {
                return new ValidationResult("Categoria inválida para receitas.");
            }

            return ValidationResult.Success;
        }
    }
}