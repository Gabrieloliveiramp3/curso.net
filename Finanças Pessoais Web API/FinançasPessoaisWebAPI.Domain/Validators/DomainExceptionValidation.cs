﻿using FinançasPessoaisWebAPI.Domain.Entities;
using FluentValidation;

namespace FinançasPessoaisWebAPI.Domain.Validators;

public class TransacaoValidator : AbstractValidator<Transacao>
{
    public TransacaoValidator()
    {
        RuleFor(e => e.Valor).NotEmpty()
             .WithMessage("O Valor não pode ser vazio");

        RuleFor(e => e.Valor).NotNull()
            .WithMessage("O Valor não pode ser nula");
    }
}

public class CategoriaValidator : AbstractValidator<Categoria>
{
    public CategoriaValidator()
    {
        RuleFor(e => e.Tipo).Must(tipo => tipo == true || tipo == false).WithMessage("O Tipo tem que ser True para Receitas e False para Despesas");
    }
}

public class UsuarioValidator : AbstractValidator<Usuario>
{
    public UsuarioValidator()
    {

        RuleFor(e => e.Transacaos).NotEmpty()
            .WithMessage("O Valor não pode ser nula");

        RuleFor(e => e.Email).EmailAddress();

        RuleFor(e => e.SenhaNormal).NotEmpty()
             .WithMessage("A Senha não pode ser vazio");

        RuleFor(e => e.SenhaNormal).NotNull()
            .WithMessage("A Senha não pode ser nula");

    }
}
