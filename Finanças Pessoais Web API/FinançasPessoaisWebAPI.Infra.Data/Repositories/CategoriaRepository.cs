﻿using FinançasPessoaisWebAPI.Domain.Entities;
using FinançasPessoaisWebAPI.Domain.Interfaces;
using FinançasPessoaisWebAPI.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;

namespace FinançasPessoaisWebAPI.Infra.Data.Repositories;

public class CategoriaRepository : ICategoriaRepository
{
    private readonly FinançasPessoaisWebAPIApiContext _categoria;

    public CategoriaRepository(FinançasPessoaisWebAPIApiContext categoria)
    {
        _categoria = categoria;
    }

    public async Task<IEnumerable<Categoria>> GetAllCategoriaAsync()
    {
        return await _categoria.Categorias.ToListAsync();
    }

    public async Task<Categoria> GetCategoriaByIdAsync(int id)
    {
        return await _categoria.Categorias.Where(x => x.Id == id).FirstOrDefaultAsync();
    }

    public async Task<bool> SaveAllAsync()
    {
        return await _categoria.SaveChangesAsync() > 0;
    }

    public async Task<Categoria> CreateCategoriaAsync(Categoria categoria)
    {
        await _categoria.Categorias.AddAsync(categoria);
        await _categoria.SaveChangesAsync();
        return categoria;
    }

    public async Task<Categoria> EditCategoriaAsync(Categoria categoria)
    {
        _categoria.Categorias.Update(categoria);
        await _categoria.SaveChangesAsync();
        return categoria;
    }

    public async Task<Categoria> RemoveCategoria(int id)
    {
        var categoria = await _categoria.Categorias.FindAsync(id);
        if (categoria != null)
        {
            _categoria.Categorias.Remove(categoria);
        }
        await _categoria.SaveChangesAsync();
        return categoria;
    }
}