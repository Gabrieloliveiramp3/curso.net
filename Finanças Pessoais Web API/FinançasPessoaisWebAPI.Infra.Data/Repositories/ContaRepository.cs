﻿using FinançasPessoaisWebAPI.Domain.Entities;
using FinançasPessoaisWebAPI.Domain.Interfaces;
using FinançasPessoaisWebAPI.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;

namespace FinançasPessoaisWebAPI.Infra.Data.Repositories;

public class ContaRepository : IContaRepository
{
    private readonly FinançasPessoaisWebAPIApiContext _conta;

    public ContaRepository(FinançasPessoaisWebAPIApiContext conta)
    {
        _conta = conta;
    }

    public async Task<IEnumerable<Conta>> GetAllContaAsync()
    {
        return await _conta.Contas
        .Include(c => c.Usuario) // Carrega o usuário associado à conta
        .ToListAsync();
    }

    public async Task<Conta> GetContaByIdAsync(int id)
    {
        return await _conta.Contas.Where(x => x.Id == id).FirstOrDefaultAsync();
    }

    public async Task<Conta> CreateContaAsync(Conta conta)
    {
        await _conta.Contas.AddAsync(conta);
        await _conta.SaveChangesAsync();
        return conta;
    }

    public async Task<Conta> EditContaAsync(Conta conta)
    {
        _conta.Contas.Update(conta);
        await _conta.SaveChangesAsync();
        return conta;
    }

    public async Task<Conta> RemoveConta(int id)
    {
        var conta = await _conta.Contas.FindAsync(id);
        if (conta != null)
        {
            _conta.Contas.Remove(conta);
        }
        await _conta.SaveChangesAsync();
        return conta;
    }
}