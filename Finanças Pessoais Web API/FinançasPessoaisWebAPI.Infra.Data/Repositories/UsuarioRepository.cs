using FinançasPessoaisWebAPI.Domain.Entities;
using FinançasPessoaisWebAPI.Domain.Interfaces;
using FinançasPessoaisWebAPI.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;

namespace FinançasPessoaisWebAPI.Infra.Data.Repositories;

public class UsuarioRepository : IUsuarioRepository
{
    private readonly FinançasPessoaisWebAPIApiContext _usuario;

    public UsuarioRepository(FinançasPessoaisWebAPIApiContext usuario)
    {
        _usuario = usuario;
    }

    public async Task<IEnumerable<Usuario>> GetAllUsuarioAsync()
    {
        return await _usuario.Usuarios.ToListAsync();
    }

    public async Task<Usuario> GetUsuarioByIdAsync(int id)
    {
        return await _usuario.Usuarios.Where(x => x.Id == id).FirstOrDefaultAsync();
    }

    public async Task<bool> SaveAllAsync()
    {
        return await _usuario.SaveChangesAsync() > 0;
    }

    public async Task<Usuario> CreateUsuarioAsync(Usuario usuario)
    {
        await _usuario.Usuarios.AddAsync(usuario);
        await _usuario.SaveChangesAsync();
        return usuario;
    }

    public async Task<Usuario> EditUsuarioAsync(Usuario usuario)
    {
        _usuario.Usuarios.Update(usuario);
        await _usuario.SaveChangesAsync();
        return usuario;
    }

    public async Task<Usuario> RemoveUsuario(int id)
    {
        var usuario = await _usuario.Usuarios.FindAsync(id);
        if (usuario != null)
        {
            _usuario.Usuarios.Remove(usuario);
        }
        await _usuario.SaveChangesAsync();
        return usuario;
    }
}