﻿using FinançasPessoaisWebAPI.Domain.Entities;
using FinançasPessoaisWebAPI.Domain.Interfaces;
using FinançasPessoaisWebAPI.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;

namespace FinançasPessoaisWebAPI.Infra.Data.Repositories;

public class TransacaoRepository : ITransacaoRepository
{
    private readonly FinançasPessoaisWebAPIApiContext _transacao;

    public TransacaoRepository(FinançasPessoaisWebAPIApiContext transacao)
    {
        _transacao = transacao;
    }

    public async Task<IEnumerable<Transacao>> GetAllTransacaoAsync()
    {
        return await _transacao.Transacaos
        .Include(c => c.Categoria) // Carrega o categoria associado a transacao
        .Include(t=> t.Conta)// Carrega o conta associado a transacao
        .Include(u=>u.Usuario)       
        .ToListAsync();

    }

    public async Task<Transacao> GetTransacaoByIdAsync(int id)
    {
        return await _transacao.Transacaos.Where(x => x.Id == id).FirstOrDefaultAsync();
    }    

    public async Task<Transacao> CreateTransacaoAsync(Transacao transacao)
    {
        await _transacao.Transacaos.AddAsync(transacao);
        await _transacao.SaveChangesAsync();
        return transacao;
    }

    public async Task<Transacao> EditTransacaoAsync(Transacao transacao)
    {
        _transacao.Transacaos.Update(transacao);
        await _transacao.SaveChangesAsync();
        return transacao;
    }

    public async Task<Transacao> RemoveTransacao(int id)
    {
        var transacao = await _transacao.Transacaos.FindAsync(id);
        if (transacao != null)
        {
            _transacao.Transacaos.Remove(transacao);
        }
        await _transacao.SaveChangesAsync();
        return transacao;
    }
}