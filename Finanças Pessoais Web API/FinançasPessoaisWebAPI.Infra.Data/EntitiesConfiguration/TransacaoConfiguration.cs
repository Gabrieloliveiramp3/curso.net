﻿using FinançasPessoaisWebAPI.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FinançasPessoaisWebAPI.Infra.Data.EntitiesConfiguration
{
    public class TransacaoConfiguration : IEntityTypeConfiguration<Transacao>
    {
        public void Configure(EntityTypeBuilder<Transacao> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.UsuarioId).IsRequired();
            builder.Property(x => x.Descricao).HasMaxLength(50);
            builder.Property(x => x.Data).IsRequired();
            builder.Property(x => x.Valor).IsRequired();

            builder.HasOne(x => x.Usuario).WithMany(x => x.Transacaos)
                .HasForeignKey(x => x.UsuarioId).OnDelete(DeleteBehavior.NoAction);

        }
    }
}