﻿using FinançasPessoaisWebAPI.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FinançasPessoaisWebAPI.Infra.Data.EntitiesConfiguration
{
    public class ContaConfiguration : IEntityTypeConfiguration<Conta>
    {
        public void Configure(EntityTypeBuilder<Conta> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.UsuarioId).IsRequired();
            builder.Property(x => x.Numero).HasMaxLength(10).IsRequired();
            builder.Property(x => x.Agencia).HasMaxLength(10).IsRequired();
            builder.Property(x => x.Banco).HasMaxLength(10).IsRequired();


        }
    }
}
