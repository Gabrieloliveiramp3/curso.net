﻿using FinançasPessoaisWebAPI.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace FinançasPessoaisWebAPI.Infra.Data.Context;

/* Aqui e feito o mapeamento que pegaram as entidades e
  irao para uma tabela no banco de dados pelo entityframework */

public class FinançasPessoaisWebAPIApiContext : DbContext
{
    public FinançasPessoaisWebAPIApiContext(DbContextOptions<FinançasPessoaisWebAPIApiContext> options)
        : base(options)
    {
    }

    /* A propriedade DbSet vai pegar a entidade Transacao e fara uma tabela no bd chamada Transacoes */

    public DbSet<Transacao> Transacaos { get; set; }
    public DbSet<Usuario> Usuarios { get; set; }
    public DbSet<Categoria> Categorias { get; set; }
    public DbSet<Conta> Contas { get; set; }

    /* Aqui estou inserindo conteudos para o preenchimento das tabelas no Banco de Dados quando ele for criado*/

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
        modelBuilder.ApplyConfigurationsFromAssembly(typeof(FinançasPessoaisWebAPIApiContext).Assembly);

        modelBuilder.Entity<Transacao>()
            .Property(e => e.Valor)
            .HasColumnType("decimal(18, 2)");

        base.OnModelCreating(modelBuilder);
        modelBuilder.Entity<Categoria>().HasData(
            new Categoria { Id = 1, categoria = "Salário", Tipo = true },
            new Categoria { Id = 2, categoria = "Benefícios", Tipo = true },
            new Categoria { Id = 3, categoria = "Vendas", Tipo = true },
            new Categoria { Id = 4, categoria = "Economias", Tipo = true },
            new Categoria { Id = 5, categoria = "Outros", Tipo = true },
            new Categoria { Id = 6, categoria = "Saúde", Tipo = false },
            new Categoria { Id = 7, categoria = "Aluguel", Tipo = false },
            new Categoria { Id = 8, categoria = "Educação", Tipo = false },
            new Categoria { Id = 9, categoria = "Lazer", Tipo = false },
            new Categoria { Id = 10, categoria = "Alimentação", Tipo = false },
            new Categoria { Id = 11, categoria = "Despesas_pessoais", Tipo = false },
            new Categoria { Id = 12, categoria = "Outros", Tipo = false });
    }
}