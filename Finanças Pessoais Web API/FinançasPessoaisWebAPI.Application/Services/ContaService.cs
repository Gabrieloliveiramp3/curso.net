﻿using AutoMapper;
using FinançasPessoaisWebAPI.Application.DTOs;
using FinançasPessoaisWebAPI.Application.Interfaces;
using FinançasPessoaisWebAPI.Domain.Entities;
using FinançasPessoaisWebAPI.Domain.Interfaces;

namespace FinançasPessoaisWebAPI.Application.Services;

public class ContaService : IContaService
{
    private readonly IContaRepository _contaRepository;
    private readonly IMapper _mapper;

    public ContaService(IContaRepository contaRepository, IMapper mapper)
    {
        _contaRepository = contaRepository;
        _mapper = mapper;
    }

    public async Task<ContaDTO> CreateContaAsync(ContaDTO contaDTO)
    {
        var conta = _mapper.Map<Conta>(contaDTO);
        var contaCriada = await _contaRepository.CreateContaAsync(conta);
        return _mapper.Map<ContaDTO>(contaCriada);
    }

    public async Task<ContaDTO> EditContaAsync(ContaDTO contaDTO)
    {
        var conta = _mapper.Map<Conta>(contaDTO);
        var contaEditada = await _contaRepository.EditContaAsync(conta);
        return _mapper.Map<ContaDTO>(contaEditada);
    }

    public async Task<IEnumerable<ContaDTO>> GetAllContaAsync()
    {
        var conta = await _contaRepository.GetAllContaAsync();
        return _mapper.Map<IEnumerable<ContaDTO>>(conta);
    }

    public async Task<ContaDTO> RemoveConta(int id)
    {
        var contaExcluida = await _contaRepository.RemoveConta(id);
        return _mapper.Map<ContaDTO>(contaExcluida);
    }

    async Task<ContaDTO> IContaService.GetContaByIdAsync(int id)
    {
        var conta = await _contaRepository.GetContaByIdAsync(id);
        return _mapper.Map<ContaDTO>(conta);
    }
}
