using System.Security.Cryptography;
using System.Text;
using AutoMapper;
using FinançasPessoaisWebAPI.Application.DTOs;
using FinançasPessoaisWebAPI.Application.Interfaces;
using FinançasPessoaisWebAPI.Domain.Entities;
using FinançasPessoaisWebAPI.Domain.Interfaces;

namespace FinançasPessoaisWebAPI.Application.Services.UsuarioService
{
    public class UsuarioService : IUsuarioService
    {
        private readonly IUsuarioRepository _usuarioRepository;
        private readonly IMapper _mapper;

        public UsuarioService(IUsuarioRepository usuarioRepository, IMapper mapper)
        {
            _usuarioRepository = usuarioRepository;
            _mapper = mapper;
        }

        public async Task<UsuarioDTO> CreateUsuarioAsync(UsuarioDTO usuarioDTO)
        {
            var usuario = _mapper.Map<Usuario>(usuarioDTO);
            if (usuarioDTO.Senha != null)
            {
                using var hmac = new HMACSHA512();
                byte[] senhaHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(usuarioDTO.Senha));
                byte[] senhaNormal = hmac.Key;

                usuario.AlterarSenha(senhaNormal, senhaHash);

            }
            var usuarioCriado = await _usuarioRepository.CreateUsuarioAsync(usuario);
            return _mapper.Map<UsuarioDTO>(usuarioCriado);
        }

        public async Task<UsuarioDTO> EditUsuarioAsync(UsuarioDTO usuarioDTO)
        {
            var usuario = _mapper.Map<Usuario>(usuarioDTO);
            var usuarioEditada = await _usuarioRepository.EditUsuarioAsync(usuario);
            return _mapper.Map<UsuarioDTO>(usuarioEditada);
        }

        public async Task<IEnumerable<UsuarioDTO>> GetAllUsuarioAsync()
        {
            var usuario = await _usuarioRepository.GetAllUsuarioAsync();
            return _mapper.Map<IEnumerable<UsuarioDTO>>(usuario);
        }

        public async Task<UsuarioDTO> RemoveUsuario(int id)
        {
            var usuarioExcluida = await _usuarioRepository.RemoveUsuario(id);
            return _mapper.Map<UsuarioDTO>(usuarioExcluida);
        }

        async Task<UsuarioDTO> IUsuarioService.GetUsuarioByIdAsync(int id)
        {
            var usuario = await _usuarioRepository.GetUsuarioByIdAsync(id);
            return _mapper.Map<UsuarioDTO>(usuario);
        }
    }
}