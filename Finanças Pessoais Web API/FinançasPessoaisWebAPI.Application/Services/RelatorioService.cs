﻿using FinançasPessoaisWebAPI.Application.Interfaces;
using FinançasPessoaisWebAPI.Domain.Entities;
using FinançasPessoaisWebAPI.Infra.Data.Context;

namespace FinançasPessoaisWebAPI.Api.Services.RelatorioService;

public class RelatorioService : IRelatorioService
{
    private readonly FinançasPessoaisWebAPIApiContext _context;

    public RelatorioService(FinançasPessoaisWebAPIApiContext context)
    {
        _context = context;
    }

    public IEnumerable<Relatorio> Relatorios()
    {
        var relatorio = (from p in _context.Transacaos
                         join c in _context.Categorias on p.CategoriaId equals c.Id
                         join d in _context.Usuarios on p.UsuarioId equals d.Id
                         group p by new { c.Tipo, d.NomeUsuario, p.Descricao, p.Data, p.Categoria, } into g
                         select new Relatorio()
                         {
                             User = g.Key.NomeUsuario,
                             Descricao = g.Key.Descricao,
                             Data = g.Key.Data,
                             tipo = g.Key.Tipo,
                             categoria = g.Key.Categoria
                         }).ToList();

        return relatorio;
    }


}