﻿using AutoMapper;
using FinançasPessoaisWebAPI.Application.DTOs;
using FinançasPessoaisWebAPI.Application.Interfaces;
using FinançasPessoaisWebAPI.Domain.Entities;
using FinançasPessoaisWebAPI.Domain.Interfaces;

namespace FinançasPessoaisWebAPI.Application.Services.TransacaoService;

public class TransacaoService : ITransacaoService
{
    private readonly ITransacaoRepository _transacaoRepository;
    private readonly IMapper _mapper;

    public TransacaoService(ITransacaoRepository transacaoRepository, IMapper mapper)
    {
        _transacaoRepository = transacaoRepository;
        _mapper = mapper;
    }

    public async Task<TransacaoPostDTO> CreateTransacaoAsync(TransacaoPostDTO transacaoPostDTO)
    {
        var transacao = _mapper.Map<Transacao>(transacaoPostDTO);
        var transacaoCriada = await _transacaoRepository.CreateTransacaoAsync(transacao);
        return _mapper.Map<TransacaoPostDTO>(transacaoCriada);
    }

    public async Task<TransacaoDTO> EditTransacaoAsync(TransacaoDTO transacaoDTO)
    {
        var transacao = _mapper.Map<Transacao>(transacaoDTO);
        var transacaoEditada = await _transacaoRepository.EditTransacaoAsync(transacao);
        return _mapper.Map<TransacaoDTO>(transacaoEditada);
    }

    public async Task<IEnumerable<TransacaoDTO>> GetAllTransacaoAsync()
    {
        var transacao = await _transacaoRepository.GetAllTransacaoAsync();
        return _mapper.Map<IEnumerable<TransacaoDTO>>(transacao);
    }

    public async Task<TransacaoDTO> RemoveTransacao(int id)
    {
        var transacaoExcluida = await _transacaoRepository.RemoveTransacao(id);
        return _mapper.Map<TransacaoDTO>(transacaoExcluida);
    }

    async Task<TransacaoDTO> ITransacaoService.GetTransacaoByIdAsync(int id)
    {
        var transacao = await _transacaoRepository.GetTransacaoByIdAsync(id);
        return _mapper.Map<TransacaoDTO>(transacao);
    }
}