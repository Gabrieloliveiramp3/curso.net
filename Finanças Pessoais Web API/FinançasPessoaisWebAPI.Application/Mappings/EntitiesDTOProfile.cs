﻿using AutoMapper;
using FinançasPessoaisWebAPI.Application.DTOs;
using FinançasPessoaisWebAPI.Domain.Entities;

namespace FinançasPessoaisWebAPI.Api.Mappings;

// Classe que define o mapeamento entre as entidades e os DTOs

public class EntitiesDTOProfile : Profile
{
    public EntitiesDTOProfile()
    {
        // Mapeia a entidade Transacao para o DTO TransacaoDTO e vice-versa
        CreateMap<Transacao, TransacaoDTO>().ReverseMap();

        CreateMap<Transacao, TransacaoPostDTO>().ReverseMap();

        // Mapeia a entidade Categoria para o DTO CategoriaDto e vice-versa
        CreateMap<Categoria, CategoriaDTO>().ReverseMap();

        CreateMap<Usuario, UsuarioDTO>().ReverseMap()
           .ForMember(t => t.SenhaNormal, opt => opt.MapFrom(x => x.Senha));

        CreateMap<Usuario, UsuarioDTO>().ReverseMap();
        CreateMap<UsuarioDTO, Usuario>().ReverseMap();
        CreateMap<Conta, ContaDTO>().ReverseMap();
    }
}

/*
- AutoMapper é uma biblioteca que facilita o mapeamento entre objetos de diferentes tipos.
- A classe EntitiesDTOProfile herda da classe Profile do AutoMapper.
- No construtor dessa classe, são definidos os mapeamentos entre as entidades e os DTOs.
- CreateMap<Transacao, TransacaoDTO>() cria um mapeamento da entidade Transacao para o DTO TransacaoDTO.
- ReverseMap() permite que o mapeamento seja bidirecional (ou seja, também do DTO para a entidade).

Essa classe é útil para manter a separação entre as entidades do banco de dados e os objetos de transferência de dados(DTOs) usados na API.O AutoMapper simplifica a conversão entre esses tipos, economizando código repetitivo.*/