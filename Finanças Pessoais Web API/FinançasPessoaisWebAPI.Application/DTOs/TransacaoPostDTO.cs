﻿using System.ComponentModel.DataAnnotations;

namespace FinançasPessoaisWebAPI.Application.DTOs;

public class TransacaoPostDTO
{
    public int UsuarioId { get; set; }

    [Required(ErrorMessage = "O valor é obrigatório.")]
    public decimal Valor { get; set; }

    public string? Descricao { get; set; }

    public int CategoriaId { get; set; }

    public int ContaId { get; set; }
}