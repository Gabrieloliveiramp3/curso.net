﻿namespace FinançasPessoaisWebAPI.Application.DTOs;

public class ContaDTO
{
    public string Id { get; set; }
    public int UsuarioId { get; set; }
    public int Agencia { get; set; }

    public int Numero { get; set; }

    public string Banco { get; set; }

    public virtual UsuarioDTO Usuario { get; set; }
}