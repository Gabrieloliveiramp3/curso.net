﻿namespace FinançasPessoaisWebAPI.Application.DTOs;

public class CategoriaDTO
{
    public int Id { get; set; }
    public string? categoria { get; set; }
    public string? Tipo { get; set; }
}