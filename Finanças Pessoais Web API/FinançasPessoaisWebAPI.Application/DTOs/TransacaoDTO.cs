﻿using System.ComponentModel.DataAnnotations;

namespace FinançasPessoaisWebAPI.Application.DTOs;

public class TransacaoDTO
{
    public int UsuarioId { get; set; }
    public int CategoriaId { get; set; }
    public int ContaId { get; set; }

    [Required(ErrorMessage = "O valor é obrigatório.")]
    public decimal Valor { get; set; }
    public string? Descricao { get; set; }
    public DateTime Data { get; set; } = DateTime.Now;

    public virtual CategoriaDTO Categoria { get; set; }
    public virtual UsuarioDTO Usuario { get; set; }
    public virtual ContaDTO Conta { get; set; }
    
}