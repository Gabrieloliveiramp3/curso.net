﻿using FinançasPessoaisWebAPI.Domain.Entities;

namespace FinançasPessoaisWebAPI.Application.Interfaces;

public interface IRelatorioService
{
    IEnumerable<Relatorio> Relatorios();
}