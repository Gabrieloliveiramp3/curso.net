﻿using FinançasPessoaisWebAPI.Application.DTOs;

namespace FinançasPessoaisWebAPI.Application.Interfaces

{
    public interface ICategoriaService
    {
        Task<IEnumerable<CategoriaDTO>> GetAllCategoriaAsync();

        Task<CategoriaDTO> GetCategoriaByIdAsync(int id);

        Task<CategoriaDTO> CreateCategoriaAsync(CategoriaDTO categoriaDTO);

        Task<CategoriaDTO> EditCategoriaAsync(CategoriaDTO categoriaDTO);

        Task<CategoriaDTO> RemoveCategoria(int id);
    }
}