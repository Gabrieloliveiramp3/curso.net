﻿using FinançasPessoaisWebAPI.Application.DTOs;

namespace FinançasPessoaisWebAPI.Application.Interfaces
{
    public interface IContaService
    {
        Task<IEnumerable<ContaDTO>> GetAllContaAsync();

        Task<ContaDTO> GetContaByIdAsync(int id);

        Task<ContaDTO> CreateContaAsync(ContaDTO contaDTO);

        Task<ContaDTO> EditContaAsync(ContaDTO contaDTO);

        Task<ContaDTO> RemoveConta(int id);
    }
}
