using FinançasPessoaisWebAPI.Application.DTOs;

namespace FinançasPessoaisWebAPI.Application.Interfaces;

public interface IUsuarioService
{
    Task<IEnumerable<UsuarioDTO>> GetAllUsuarioAsync();

    Task<UsuarioDTO> GetUsuarioByIdAsync(int id);

    Task<UsuarioDTO> CreateUsuarioAsync(UsuarioDTO usuarioDTO);

    Task<UsuarioDTO> EditUsuarioAsync(UsuarioDTO usuarioDTO);

    Task<UsuarioDTO> RemoveUsuario(int id);


}