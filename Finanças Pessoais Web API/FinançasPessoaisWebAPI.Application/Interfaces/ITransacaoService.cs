﻿using FinançasPessoaisWebAPI.Application.DTOs;

namespace FinançasPessoaisWebAPI.Application.Interfaces;

public interface ITransacaoService
{
    Task<IEnumerable<TransacaoDTO>> GetAllTransacaoAsync();

    Task<TransacaoDTO> GetTransacaoByIdAsync(int id);

    Task<TransacaoPostDTO> CreateTransacaoAsync(TransacaoPostDTO transacaoPutDTO);

    Task<TransacaoDTO> EditTransacaoAsync(TransacaoDTO transacaoDTO);

    Task<TransacaoDTO> RemoveTransacao(int id);
}