﻿using System.Security.Claims;

namespace FinançasPessoaisWebAPI.Infra.Ioc;

public static class ClaimsExtension
{
    public static int GetId(this ClaimsPrincipal user)
    {
        return int.Parse(user.FindFirst("id").Value);
    }

    public static string GetEmail(this ClaimsPrincipal user)
    {
        return user.FindFirst("email").Value;
    }
}