﻿using FinançasPessoaisWebAPI.Api.Mappings;
using FinançasPessoaisWebAPI.Api.Services.RelatorioService;
using FinançasPessoaisWebAPI.Application.Interfaces;
using FinançasPessoaisWebAPI.Application.Services;
using FinançasPessoaisWebAPI.Application.Services.CategoriaService;
using FinançasPessoaisWebAPI.Application.Services.TransacaoService;
using FinançasPessoaisWebAPI.Application.Services.UsuarioService;
using FinançasPessoaisWebAPI.Domain.Account;
using FinançasPessoaisWebAPI.Domain.Interfaces;
using FinançasPessoaisWebAPI.Infra.Data.Context;
using FinançasPessoaisWebAPI.Infra.Data.Identity;
using FinançasPessoaisWebAPI.Infra.Data.Repositories;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace FinançasPessoaisWebAPI.Infra.Ioc;

public static class DependecyInjection
{
    public static IServiceCollection AddInfrastructure(this IServiceCollection services,
        IConfiguration configuration)
    {
        services.AddDbContext<FinançasPessoaisWebAPIApiContext>(options =>
        {
            options.UseSqlServer(configuration.GetConnectionString("DefaultConnection"),
            b => b.MigrationsAssembly(typeof(FinançasPessoaisWebAPIApiContext).Assembly.FullName));
        });

        services.AddAuthentication(opt =>
        {
            opt.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            opt.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
        }
        ).AddJwtBearer(options =>
        {
            options.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidateAudience = true,
                ValidateLifetime = true,
                ValidateIssuerSigningKey = true,

                ValidIssuer = configuration["jwt:issuer"],
                ValidAudience = configuration["jwt:audience"],
                IssuerSigningKey = new SymmetricSecurityKey(
                    Encoding.UTF8.GetBytes(configuration["jwt:secretKey"])),
                ClockSkew = TimeSpan.Zero
            };
        });

        //Services

        services.AddScoped<ITransacaoService, TransacaoService>();
        services.AddScoped<ICategoriaService, CategoriaService>();
        services.AddScoped<IUsuarioService, UsuarioService>();
        services.AddScoped<IRelatorioService, RelatorioService>();
        services.AddScoped<IContaService, ContaService>();
        services.AddScoped<IAuthenticate, AuthenticateService>();

        //Mapper
        services.AddAutoMapper(typeof(EntitiesDTOProfile));

        //Repository

        services.AddScoped<ITransacaoRepository, TransacaoRepository>();
        services.AddScoped<ICategoriaRepository, CategoriaRepository>();
        services.AddScoped<IUsuarioRepository, UsuarioRepository>();
        services.AddScoped<IContaRepository, ContaRepository>();

        return services;
    }
}