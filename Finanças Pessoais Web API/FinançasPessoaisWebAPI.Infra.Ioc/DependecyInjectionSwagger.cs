﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;


namespace FinançasPessoaisWebAPI.Infra.Ioc;

public static class DependecyInjectionSwagger
{
    public static IServiceCollection AddInfrastructureSwagger(this IServiceCollection services)
    {
        services.AddSwaggerGen(c =>
        {
            // Define uma definição de segurança para tokens Bearer
            c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
            {
                Name = "Authorization",
                Type = SecuritySchemeType.ApiKey,
                BearerFormat = "JWT",
                In = ParameterLocation.Header,
                Description = "JSON Web Tokens são um método aberto RFC 7519 padrão do setor para representar reivindicações com segurança entre duas partes."
            });
                     

            // Especifica o requisito de segurança para tokens Bearer
            c.AddSecurityRequirement(new OpenApiSecurityRequirement
            {
                {
                    new OpenApiSecurityScheme
                    {
                        Reference = new OpenApiReference
                        {
                            Type = ReferenceType.SecurityScheme,
                            Id = "Bearer"
                        }
                    },
                    new string[] {}
                }
            });
        });

        return services;
    }
}





