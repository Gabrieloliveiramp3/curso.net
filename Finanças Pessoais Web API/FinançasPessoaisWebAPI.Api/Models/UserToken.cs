﻿using Microsoft.AspNetCore.Mvc;

namespace FinançasPessoaisWebAPI.Api.Models;

public class UserToken 
{
    public string Token { get; set; }
}