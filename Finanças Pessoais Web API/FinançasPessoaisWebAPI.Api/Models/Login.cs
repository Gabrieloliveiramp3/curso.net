﻿using OpenXmlPowerTools;
using System.ComponentModel.DataAnnotations;

namespace FinançasPessoaisWebAPI.Api.Models
{
    public class Login
    {
        [Required(ErrorMessage = "0 e-mail e obrigatório")]
        [DataType(DataType.EmailAddress)]
        public required string Email { get; set; }

        [Required(ErrorMessage = "A senha e obrigatória")]
        [DataType(DataType.Password)]
        public required string Senha { get; set; }

        
    }
}