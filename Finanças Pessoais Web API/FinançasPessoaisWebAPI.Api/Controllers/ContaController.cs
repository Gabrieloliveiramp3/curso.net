﻿using FinançasPessoaisWebAPI.Application.DTOs;
using FinançasPessoaisWebAPI.Application.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FinançasPessoaisWebAPI.Api.Controllers;

[ApiController]
[Route("[controller]")]
[Authorize]
public class ContaController : Controller
{
    private readonly IContaService _contaService;

    public ContaController(IContaService contaService)
    {
        _contaService = contaService;
    }

    /// <summary>
    /// Busca uma conta pelo id.
    /// </summary>
    /// <param name="id">Identificador da conta bancária.</param>
    /// <returns>Retorna a conta  encontrada, caso contrário NotFound.</returns>
    [HttpGet]
    [Route("{id}")]
    public async Task<IActionResult> GetContaByIdAsync(int id)
    {
        var contaDTOGetId = await _contaService.GetContaByIdAsync(id);
        if (contaDTOGetId == null)
        {
            return NotFound("Conta não encontrada");
        }
        return Ok(contaDTOGetId);
    }

    /// <summary>
    /// Recupera todas as contas  cadastradas.
    /// </summary>
    /// <returns>Retorna a lista de contas bancárias.</returns>
    [HttpGet]
    public async Task<IActionResult> GetAllContaAsync()
    {
        var contaDTOGetId = await _contaService.GetAllContaAsync();

        return Ok(contaDTOGetId);
    }

    /// <summary>
    /// Insere uma nova conta .
    /// </summary>
    /// <param name="contaDTO">Objeto contendo as informações da conta bancária.</param>
    /// <returns>Retorna Ok se a conta for criada, caso contrário BadRequest.</returns>
    [HttpPost]
    public async Task<IActionResult> CreateContaAsync(ContaDTO contaDTO)
    {
        var contaDTOCreate = await _contaService.CreateContaAsync(contaDTO);
        if (contaDTOCreate == null)
        {
            return BadRequest("Ocorreu um erro ao criar uma Conta");
        }
        return Ok("Transação criada com sucesso");
    }

    /// <summary>
    /// Edita uma conta existente.
    /// </summary>
    /// <param name="contaDTO">Objeto contendo as informações atualizadas da conta .</param>
    /// <returns>Retorna Ok se a conta for editada, caso contrário BadRequest.</returns>
    [HttpPut]
    public async Task<IActionResult> EditContaAsync(ContaDTO contaDTO)
    {
        var contaDTOEdit = await _contaService.EditContaAsync(contaDTO);
        if (contaDTOEdit == null)
        {
            return BadRequest("Ocorreu um erro ao editar uma Conta");
        }
        return Ok("Conta editada com sucesso");
    }

    /// <summary>
    /// Remove uma conta pelo id.
    /// </summary>
    /// <param name="id">Identificador da conta bancária.</param>
    /// <returns>Retorna Ok se a conta for excluída, caso contrário BadRequest.</returns>
    [HttpDelete]
    public async Task<IActionResult> RemoveConta(int id)
    {
        var contaDTORemove = await _contaService.RemoveConta(id);
        if (contaDTORemove == null)
        {
            return BadRequest("Ocorreu um erro ao excluir uma Conta");
        }
        return Ok("Conta excluída com sucesso");
    }
}