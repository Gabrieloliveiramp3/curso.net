﻿using FinançasPessoaisWebAPI.Application.DTOs;
using FinançasPessoaisWebAPI.Application.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FinançasPessoaisWebAPI.Api.Controllers;

[ApiController]
[Route("[controller]")]
[Authorize]
public class CategoriaController : Controller
{
    private readonly ICategoriaService _categoriaService;

    public CategoriaController(ICategoriaService categoriaService)
    {
        _categoriaService = categoriaService;
    }

    /// <summary>
    /// Obtém uma categoria pelo ID.
    /// </summary>
    /// <param name="id">ID da categoria.</param>
    /// <returns>Detalhes da categoria.</returns>
    [HttpGet]
    [Route("{id}")]
    public async Task<IActionResult> GetCategoriaByIdAsync(int id)
    {
        var categoriaDTOGetId = await _categoriaService.GetCategoriaByIdAsync(id);
        if (categoriaDTOGetId == null)
        {
            return NotFound("Categoria não encontrada");
        }
        return Ok(categoriaDTOGetId);
    }

    /// <summary>
    /// Obtém todas as categorias.
    /// </summary>
    /// <returns>Lista de categorias.</returns>
    [HttpGet]
    public async Task<IActionResult> GetAllCategoriaAsync()
    {
        var categoriaDTOGetId = await _categoriaService.GetAllCategoriaAsync();

        return Ok(categoriaDTOGetId);
    }

    /// <summary>
    /// Cria uma nova categoria.
    /// </summary>
    /// <param name="categoriaDTO">Dados da categoria.</param>
    /// <returns>Mensagem de sucesso ou erro.</returns>
    [HttpPost]
    public async Task<IActionResult> CreateCategoriaAsync(CategoriaDTO categoriaDTO)
    {
        var categoriaDTOCreate = await _categoriaService.CreateCategoriaAsync(categoriaDTO);
        if (categoriaDTOCreate == null)
        {
            return BadRequest("Ocorreu um erro ao criar uma categoria");
        }
        return Ok("Categoria criada com sucesso");
    }

    /// <summary>
    /// Edita uma categoria existente.
    /// </summary>
    /// <param name="categoriaDTO">Dados da categoria.</param>
    /// <returns>Mensagem de sucesso ou erro.</returns>
    [HttpPut]
    public async Task<IActionResult> EditCategoriaAsync(CategoriaDTO categoriaDTO)
    {
        var categoriaDTOEdit = await _categoriaService.EditCategoriaAsync(categoriaDTO);
        if (categoriaDTOEdit == null)
        {
            return BadRequest("Ocorreu um erro ao editar uma categoria");
        }
        return Ok("Categoria editada com sucesso");
    }

    /// <summary>
    /// Remove uma categoria pelo ID.
    /// </summary>
    /// <param name="id">ID da categoria.</param>
    /// <returns>Mensagem de sucesso ou erro.</returns>
    [HttpDelete]
    public async Task<IActionResult> RemoveCategoria(int id)
    {
        var categoriaDTORemove = await _categoriaService.RemoveCategoria(id);
        if (categoriaDTORemove == null)
        {
            return BadRequest("Ocorreu um erro ao excluir uma categoria");
        }
        return Ok("Categoria excluída com sucesso");
    }
}