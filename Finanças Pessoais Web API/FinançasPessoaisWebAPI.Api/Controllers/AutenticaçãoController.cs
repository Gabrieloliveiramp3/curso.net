﻿using FinançasPessoaisWebAPI.Api.Models;
using FinançasPessoaisWebAPI.Application.DTOs;
using FinançasPessoaisWebAPI.Application.Interfaces;
using FinançasPessoaisWebAPI.Domain.Account;
using Microsoft.AspNetCore.Mvc;

namespace FinançasPessoaisWebAPI.Api.Controllers;

[Route("[controller]")]
[ApiController]
public class AutenticaçãoController : ControllerBase
{
    private readonly IUsuarioService _usuarioService;
    private readonly IAuthenticate _authenticateService;

    public AutenticaçãoController(IUsuarioService usuarioService, IAuthenticate authenticateService)
    {
        _usuarioService = usuarioService;
        _authenticateService = authenticateService;
    }

    [HttpPost]
    [Route("/Registrar")]
    public async Task<ActionResult<UserToken>> CreateUsuarioAsync(UsuarioDTO usuarioDTO)
    {
        if (usuarioDTO == null)
        {
            return BadRequest("Ocorreu um erro ao criar um usuário");
        }

        var emailExiste = await _authenticateService.UserExistAsync(usuarioDTO.Email);

        if (emailExiste)
        {
            return BadRequest("E-mail já possui um cadastro.");
        }

        var usuario = await _usuarioService.CreateUsuarioAsync(usuarioDTO);
        if (usuario == null)
        {
            return BadRequest("Erro ao cadastrar");
        }

        var token = _authenticateService.GenerateToken(usuario.Id, usuario.Email);

        return new UserToken
        {
            Token = token
        };
    }

    [HttpPost]
    [Route("/Login")]
    public async Task<ActionResult<UserToken>> Selecionar(Login login)
    {
        var existe = await _authenticateService.UserExistAsync(login.Email);
        if (!existe)
        {
            return Unauthorized("Usuário non ecziste.");
        }

        var result = await _authenticateService.AuthenticateAsync(login.Email, login.Senha);

        if (!result)
        {
            return Unauthorized("Usuário ou senha inválida.");
        }

        var usuario = await _authenticateService.GetUserByEmail(login.Email);
        var token = _authenticateService.GenerateToken(usuario.Id, usuario.Email);

        return new UserToken
        {
            Token = token
        };
    }
}