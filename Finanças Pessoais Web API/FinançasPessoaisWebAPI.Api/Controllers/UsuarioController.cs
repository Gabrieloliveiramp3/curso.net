﻿using FinançasPessoaisWebAPI.Application.DTOs;
using FinançasPessoaisWebAPI.Application.Interfaces;
using FinançasPessoaisWebAPI.Domain.Account;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FinançasPessoaisWebAPI.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsuarioController : Controller
    {
        private readonly IUsuarioService _usuarioService;
        private readonly IAuthenticate _authenticateService;

        public UsuarioController(IUsuarioService usuarioService, IAuthenticate authenticateService)
        {
            _usuarioService = usuarioService;
            _authenticateService = authenticateService;
        }

        /// <summary>
        /// Obtém um usuário pelo ID.
        /// </summary>
        /// <param name="id">ID do usuário.</param>
        /// <returns>Detalhes do usuário.</returns>
        [HttpGet]
        [Route("{id}")]
        [Authorize]
        public async Task<IActionResult> GetUsuarioByIdAsync(int id)
        {
            var usuarioDTOGetId = await _usuarioService.GetUsuarioByIdAsync(id);
            if (usuarioDTOGetId == null)
            {
                return NotFound("Usuário não encontrado");
            }

            return Ok(usuarioDTOGetId);
        }

        /// <summary>
        /// Obtém todos os usuários.
        /// </summary>
        /// <returns>Lista de usuários.</returns>
        [HttpGet]
        [Authorize]
        public async Task<IActionResult> GetAllUsuarioAsync()
        {
            var usuarioDTOGetId = await _usuarioService.GetAllUsuarioAsync();

            return Ok(usuarioDTOGetId);
        }

        /// <summary>
        /// Cria um novo usuário.
        /// </summary>
        /// <param name="usuarioDTO">Dados do usuário.</param>
        /// <returns>Mensagem de sucesso ou erro.</returns>

        /// <summary>
        /// Edita um usuário existente.
        /// </summary>
        /// <param name="usuarioDTO">Dados do usuário.</param>
        /// <returns>Mensagem de sucesso ou erro.</returns>
        [HttpPut]
        [Authorize]
        public async Task<IActionResult> EditUsuarioAsync(UsuarioDTO usuarioDTO)
        {
            var usuarioDTOEdit = await _usuarioService.EditUsuarioAsync(usuarioDTO);
            if (usuarioDTOEdit == null)
            {
                return BadRequest("Ocorreu um erro ao editar um usuário");
            }
            return Ok("Usuário editado com sucesso");
        }

        /// <summary>
        /// Remove um usuário pelo ID.
        /// </summary>
        /// <param name="id">ID do usuário.</param>
        /// <returns>Mensagem de sucesso ou erro.</returns>
        [HttpDelete]
        [Authorize]
        public async Task<IActionResult> RemoveUsuario(int id)
        {
            var usuarioDTORemove = await _usuarioService.RemoveUsuario(id);
            if (usuarioDTORemove == null)
            {
                return BadRequest("Ocorreu um erro ao excluir um usuário");
            }
            return Ok("Usuário excluído com sucesso");
        }
    }
}