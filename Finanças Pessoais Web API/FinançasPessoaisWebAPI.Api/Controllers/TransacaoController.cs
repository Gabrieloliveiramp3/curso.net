﻿using FinançasPessoaisWebAPI.Application.DTOs;
using FinançasPessoaisWebAPI.Application.Interfaces;
using FinançasPessoaisWebAPI.Infra.Ioc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FinançasPessoaisWebAPI.Api.Controllers;

[ApiController]
[Route("[controller]")]
[Authorize]
public class TransacaoController : Controller
{
    private readonly ITransacaoService _transacaoService;
    private readonly IUsuarioService _usuarioService;

    public TransacaoController(ITransacaoService transacaoService, IUsuarioService usuarioService)
    {
        _transacaoService = transacaoService;
        _usuarioService = usuarioService;
    }


    /// <summary>
    /// Obtém uma transação pelo ID.
    /// </summary>
    /// <param name="id">ID da transação.</param>
    /// <returns>Detalhes da transação.</returns>
    [HttpGet]
    [Route("{id}")]
    public async Task<IActionResult> GetTransacaoByIdAsync(int id)
    {
        var transacaoDTOGetId = await _transacaoService.GetTransacaoByIdAsync(id);
        if (transacaoDTOGetId == null)
        {
            return NotFound("Transacao não encontrada");
        }
        return Ok(transacaoDTOGetId);
    }

    /// <summary>
    /// Obtém todas as transações.
    /// </summary>
    /// <returns>Lista de transações.</returns>
    [HttpGet]
    public async Task<IActionResult> GetAllTransacaoAsync()
    {

        var transacaoDTOGetId = await _transacaoService.GetAllTransacaoAsync();

        return Ok(transacaoDTOGetId);
    }

    /// <summary>
    /// Cria uma nova transação.
    /// </summary>
    /// <param name="transacaoDTO">Dados da transação.</param>
    /// <returns>Mensagem de sucesso ou erro.</returns>
    [HttpPost]
    public async Task<IActionResult> CreateTransacaoAsync(TransacaoPostDTO transacaoPostDTO)
    {
        var transacaoDTOCreate = await _transacaoService.CreateTransacaoAsync(transacaoPostDTO);
        if (transacaoDTOCreate == null)
        {
            return BadRequest("Ocorreu um erro ao criar uma transacao");
        }
        return Ok("Transacao criada com sucesso");
    }

    /// <summary>
    /// Edita uma transação existente.
    /// </summary>
    /// <param name="transacaoDTO">Dados da transação.</param>
    /// <returns>Mensagem de sucesso ou erro.</returns>
    [HttpPut]
    public async Task<IActionResult> EditTransacaoAsync(TransacaoDTO transacaoDTO)
    {
        var transacaoDTOEdit = await _transacaoService.EditTransacaoAsync(transacaoDTO);
        if (transacaoDTOEdit == null)
        {
            return BadRequest("Ocorreu um erro ao editar uma transacao");
        }
        return Ok("Transacao editada com sucesso");
    }

    /// <summary>
    /// Remove uma transação pelo ID.
    /// </summary>
    /// <param name="id">ID da transação.</param>
    /// <returns>Mensagem de sucesso ou erro.</returns>
    [HttpDelete]
    public async Task<IActionResult> RemoveTransacao(int id)
    {
        var userId = User.GetId();
        var usuario = await _usuarioService.GetUsuarioByIdAsync(userId);

        if (!usuario.Admin) 
        {
            return Unauthorized("Somente os Semi-Deuses podem excluir algo");
        }

        var transacaoDTORemove = await _transacaoService.RemoveTransacao(id);
        if (transacaoDTORemove == null)
        {
            return BadRequest("Ocorreu um erro ao excluir uma transacao");
        }
        return Ok("Transacao excluída com sucesso");
    }
}