﻿using FinançasPessoaisWebAPI.Domain.Validators;
using FinançasPessoaisWebAPI.Infra.Data.Context;
using FinançasPessoaisWebAPI.Infra.Ioc;
using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using System.Reflection;
using System.Text.Json.Serialization;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddValidatorsFromAssemblyContaining<UsuarioValidator>();
builder.Services.AddValidatorsFromAssemblyContaining<CategoriaValidator>();
builder.Services.AddValidatorsFromAssemblyContaining<TransacaoValidator>();
builder.Services.AddFluentValidationAutoValidation();

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();

builder.Services.AddInfrastructureSwagger();
builder.Services.AddInfrastructure(builder.Configuration);

builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo
    {
        Title = "Liso Tracker WebAPI",
        Version = "v1",
        Description = " \"Se você não comprar nada, o desconto é maior.\" ",
        Contact = new OpenApiContact
        {
            Name = "Gabriel Oliveira - Alessandro Beserra - Fabio Ricardo - Jose Victor - Wana Batista",

            Url = new Uri("https://gitlab.com/Gabrieloliveiramp3/curso.net/-/tree/main/Finan%C3%A7as%20Pessoais%20Web%20API?ref_type=heads")
        }
    });
    var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
    c.IncludeXmlComments(xmlPath);
   

});


builder.Services.AddControllers().AddJsonOptions(x =>
{
    x.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
});

builder.Services.AddDbContext<FinançasPessoaisWebAPIApiContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")).EnableDetailedErrors());

builder.Services.AddSqlite<FinançasPessoaisWebAPIApiContext>("Data Source=FinançasPessoais.db");

var app = builder.Build();
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
else
{
    app.UseExceptionHandler("/Error");
    app.UseHsts();
}
app.UseHttpsRedirection();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();