﻿using Hierarquia_de_Formas_Geométricas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

internal class Program
{
    private static void Main()
    {
        Retangulo retangulo = new Retangulo(baseRetangulo: 5, altura: 3);
        Console.WriteLine($"Área do retângulo: {retangulo.CalcularArea()}");
        Console.WriteLine($"Perímetro do retângulo: {retangulo.CalcularPerimetro()}\n");

        Circulo circulo = new Circulo(raio: 4);
        Console.WriteLine($"\nÁrea do círculo: {circulo.CalcularArea()}");
        Console.WriteLine($"Perímetro do círculo: {circulo.CalcularPerimetro()}\n");

        Triangulo triangulo = new Triangulo(lado1: 3, lado2: 4, lado3: 5);
        Console.WriteLine($"\nÁrea do triângulo: {triangulo.CalcularArea()}");
        Console.WriteLine($"Perímetro do triângulo: {triangulo.CalcularPerimetro()}\n");

        List<FormaGeometrica> formas = new List<FormaGeometrica>
        {
            new Retangulo(baseRetangulo: 5, altura: 3),
            new Circulo(raio: 4),
            new Triangulo(lado1: 3, lado2: 4, lado3: 5)
        };

        foreach (var forma in formas)
        {
            Console.WriteLine($"\nÁrea: {forma.CalcularArea()}");
            Console.WriteLine($"Perímetro: {forma.CalcularPerimetro()}");
        }
    }
}